﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webSite.Models;

namespace webSite.Controllers
{
    public class AppController : Controller
    {
        PlatformDb _db = new PlatformDb();
        // GET: App
        public ActionResult Index()
        {
            var model = _db.Apps.ToList();
            return View(model);
        }

        // GET: App/Details/5
        public ActionResult Details(int id)
        {
            App App = _db.Apps.Find(id);
            if (App == null)
            {
                return HttpNotFound();
            }

            return View(App);
        }

        // GET: App/Create
        public ActionResult Create()
        {
            return View();
        }

        // App: App/Create
        [HttpPost]
        public ActionResult Create(App App)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Apps.Add(App);
                    _db.SaveChanges();

                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: App/Edit/5
        public ActionResult Edit(int id)
        {
            App App = _db.Apps.Find(id);
            if (App == null)
            {
                return HttpNotFound();
            }

            return View(App);
        }

        // App: App/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, App App)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(App).State = EntityState.Modified;
                    _db.SaveChanges();

                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: App/Delete/5
        public ActionResult Delete(int id)
        {
            App App = _db.Apps.Find(id);
            if (App == null)
            {
                return HttpNotFound();
            }

            return View(App);
        }

        // App: App/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, App App)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(App).State = EntityState.Deleted;
                    _db.SaveChanges();

                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

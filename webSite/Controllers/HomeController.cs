﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using webSite.Models;
using Microsoft.AspNet.Identity;


namespace webSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        public ActionResult UserProfile()
        {
            var db = new ApplicationDbContext();
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).Select(u => u).ToList();

            ViewBag.users = user;
            return View();
        }

        [Authorize]
        public ActionResult Members()
        {
            var db = new ApplicationDbContext();
            var users = db.Users.ToList();

            string[] emailConfirmed = Request.QueryString.GetValues("EmailConfirmed");

            if (emailConfirmed != null)
            {
                for (int i = 0; i < users.Count; i++)
                {
                    users[i].EmailConfirmed = emailConfirmed.Contains(users[i].Id) ? true : false;
                }
                db.SaveChanges();
            }

            string[] phoneNumber = Request.QueryString.GetValues("PhoneNumber");

            if (phoneNumber != null)
            {
                for (int i = 0; i < users.Count; i++)
                {
                    users[i].PhoneNumber = phoneNumber[i];
                }
                var newUser = new ApplicationUser();
                newUser.UserName = "test@gmail.com";
                users.Add(newUser);
                db.SaveChanges();
            }

            //string[] NickName = Request.QueryString.GetValues("NickName");

            //if (NickName != null)
            //{
            //    for (int i = 0; i < users.Count; i++)
            //    {
            //        users[i].NickName = NickName[i];
            //    }
            //    db.SaveChanges();
            //}

            ViewBag.users = users;

            return View(users);


        }

        [Authorize]
        public ActionResult CreateNewUser()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Menu()
        {
            ViewBag.Message = "Your application description page.";

            return View();





        }

    }
}
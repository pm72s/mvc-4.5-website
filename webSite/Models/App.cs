﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webSite.Models
{
    public class App
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string imageUrl { get; set; }
        public string author { get; set; }
        public DateTime released { get; set; }
        public DateTime updated { get; set; }
    }
}
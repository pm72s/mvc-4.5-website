﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace webSite.Models
{
    public class PlatformDb : DbContext
    {
        public PlatformDb() : base ("name=DefaultConnection")
        {

        }
        public DbSet<Post> Posts { get; set; }
        public DbSet<App> Apps { get; set; }
    }
}